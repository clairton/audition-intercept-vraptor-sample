package br.eti.clairton.audition.interceptor;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.BeforeCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.controller.ControllerMethod;

@Intercepts
@RequestScoped
public class AuditionInterceptor {

	private HttpServletRequest request;

	private ControllerMethod method;

	@Deprecated
	public AuditionInterceptor() {
		this(null, null);
	}

	@Inject
	public AuditionInterceptor(final HttpServletRequest request, final ControllerMethod method) {
		this.request = request;
		this.method = method;
	}

	@BeforeCall
	public void before() {
		System.err.println("Controller: " + method.getController().getType());
		System.err.println("Controller Method: " + method.getMethod());
		System.err.println("Request Headers: " + request.getHeaderNames());
	}
}
