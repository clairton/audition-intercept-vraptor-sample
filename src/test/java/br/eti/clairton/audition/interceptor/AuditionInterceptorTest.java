package br.eti.clairton.audition.interceptor;

import javax.enterprise.inject.Produces;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;

import br.com.caelum.vraptor.test.VRaptorIntegration;

public class AuditionInterceptorTest extends VRaptorIntegration {

	@Produces
	public Validator getValidator(final ValidatorFactory factory) {
		return factory.getValidator();
	}

	@Produces
	public ValidatorFactory getValidatorFactory() {
		return Validation.buildDefaultValidatorFactory();
	}

	@Test
	public void testPing() {
		navigate().get("/ping").execute().wasStatus(204);
	}

	@Test
	public void testPong() {
		navigate().post("/pong").execute().wasStatus(204);
	}

}
