package br.eti.clairton.audition.interceptor;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;

@Controller
@Path("")
public class PingController {

	private Result result;

	@Deprecated
	public PingController() {
		this(null);
	}

	@Inject
	public PingController(final Result result) {
		this.result = result;
	}

	@Get("/ping")
	public void ping() {
		System.err.println("Ping!!!");
		result.use(Results.http()).setStatusCode(204);
	}

	@Post("/pong")
	public void pong() {
		System.err.println("post!!!");
		result.use(Results.http()).setStatusCode(204);
	}
}
